from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import *
from crispy_forms.helper import FormHelper
from django import forms


class PreguntaForm(ModelForm):
    class Meta:
        model = Pregunta
        fields = '__all__'


class EmpleadoForm(ModelForm):
    class Meta:
        model = Empleado
        fields = '__all__'



class EncuestaForm(ModelForm):

    class Meta:
        model = Encuesta
        fields = '__all__'
        widgets = {
            'fecha_inicio': forms.DateInput(attrs={'class': 'datepicker', 'id': 'data_input'}),
            'fecha_termino': forms.DateInput(attrs={'class': 'datepicker2', 'id': 'data_input'}),
            'empleados': forms.CheckboxSelectMultiple,
        }


class DominioForm(ModelForm):
    class Meta:
        model = Dominio
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DominioForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        if self.instance.id:
            self.fields['nombre'].widget.attrs['readonly'] = True


class ParametrosForm(ModelForm):
    class Meta:
        model = Parametros
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ParametrosForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False 

class ExtendUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = (
            'username',
            'email',
            # 'first_name',
            # 'last_name',
            'password1',
            'password2',
        )
    def save(self, commit=True):
        user = super().save(commit=True)

        user.email = self.cleaned_data['email']
        # user.first_name = self.cleaned_data['first_name']
        # user.last_name = self.cleaned_data['last_name'] 

        if commit:
            user.save()
        else:
            print("no save pero save iwal")
            user.save()
        return user

class UserEmpleadoExtend(forms.ModelForm):
    class Meta:
        model = Empleado

        fields = (
            'primer_nombre',
            'segundo_nombre',
            'apellido_materno',
            'apellido_paterno',
            # 'email',
            'rut',
            'cargo',
            'sucursal',
            'tipo',
        )
        
        # self.helper.form_show_labels = False


CHOICES = (
    ('s', 'Si'),
    ('n', 'No'),
    ('d', 'Desconozco'),
)


class AnswerForm(ModelForm):
    choice_text = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())

    class Meta:
        model = Alternativas
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
