from django.conf.urls import url
from django.urls import path, include
from . import views

app_name = 'gap'



pregunta_urlpatterns = [
    path('', views.PreguntaListView.as_view(), name='pregunta_list'),
    path('new', views.PreguntaCreate.as_view(), name='pregunta_new'),
    path('edit/<int:pk>', views.PreguntaUpdate.as_view(), name='pregunta_edit'),
    path('<int:pk>', views.PreguntaDetailView.as_view(), name='pregunta_detail'),
    path('delete/<int:pk>', views.PreguntaDelete.as_view(), name='pregunta_delete'),
]

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('responsables/', views.Responsables, name='responsables'),
    path('preguntas/', include(pregunta_urlpatterns)),
    path('carga_usuario/', views.CargaUsuario.as_view(), name='carga_usuario'),
    path('singleuser/', views.empleado_create, name='carga_usuario_singular'),
    path('multipleuser/', views.CargaMasivaUsuario, name='carga_usuario_masiva'),
    path('parametros/<int:pk>', views.ParametrosUpdate.as_view(), name='parametros'),
    path('resultados/', views.Resultados, name='Resultados'),
    path('homencuestas/<int:pk>', views.homecuestas, name='homecuestas'),
    path('encuesta/<int:enc_pk>/<int:dom_pk>', views.encuesta, name='encuesta'),
    path('exportResultados/<int:pk>/', views.Returnexcelresultados, name='exportResultados'),
    path('recomendaciones/', views.Recomendaciones, name='recomendaciones'),
    path('informe/<int:pk>/', views.generarInforme, name='informe'),
    path('plandeaccion/<int:pk>/', views.generarPlanDeAccion, name='PlanDeAccion'),
    path('crearencuesta/', views.EncuestaCreate, name='CrearEncuesta'),
    path('graficos/', views.Graficos, name='Graficos'),
    path('encuestas/', views.EncuestasShow, name='EncuestasShow'),
    path('preguntascarga/', views.CargaMasivaPregunta, name='preguntas'),

]



