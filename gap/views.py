from django.views.generic.base import TemplateView
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.shortcuts import render,redirect
import logging
import string
from datetime import timedelta
import numpy
import xlwt
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.db.models import Count
from django.forms import modelformset_factory
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import Permission
from django.contrib.auth import authenticate
from .models import(
                    Departamento,
                    Sucursal,
                    Empleado,
                    Pregunta,
                    Alternativas,
                    Parametros,
                    Dominio
                )
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.base import TemplateView
from django.template.loader import render_to_string

from weasyprint import HTML

from .forms import *


# Create your views here.

class Home(TemplateView):
    template_name = "gap/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['latest_articles'] = Article.objects.all()[:5] example
        return context


# Opciones de carga de usuario
class CargaUsuario(TemplateView):
    template_name = 'gap/carga_usuario.html'
    
def empleado_create(request):
    template = 'gap/carga_usuario_singular.html'

    if request.method == "POST":
        form = ExtendUserCreationForm(request.POST)
        empleado_extend = UserEmpleadoExtend(request.POST)

        if form.is_valid() and empleado_extend.is_valid():
            user = form.save()
            
            content_type = ContentType.objects.get_for_model(Empleado)
            # permission = Permission.objects.get(
            #         codename='is_employ',
            #         content_type=content_type,
            #     )
            # user.user_permissions.add(permission)
                
            employ_profile = empleado_extend.save(commit=False)
            employ_profile.user = user
        
            employ_profile.save()

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username,password=password)


            return redirect('gap:carga_usuario')
       
    else:
        form = ExtendUserCreationForm()
        empleado_extend = UserEmpleadoExtend()
    
    context = {
        'form': form,
        'empleado_extend': empleado_extend,
    }
    return render(request, template, context)       


class SingleUser(generic.CreateView):
    model = Empleado
    fields = ['primer_nombre', 'segundo_nombre', 'apellido_materno', 'apellido_paterno', 'email', 'rut', 'cargo',
              'sucursal', 'tipo']
    success_url = reverse_lazy('gap:home')
    template_name = 'gap/carga_usuario_singular.html'


# Carga Usuario Masiva
def CargaMasivaUsuario(request):
    data = {}
    error = False
    if "GET" == request.method:
        return render(request, "gap/carga_usuario_masiva.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'Archivo no es de formato CSV')
            error = True;
            return HttpResponseRedirect(reverse_lazy("gap:carga_usuario_masiva"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Archivo es excede el peso permitido (%.2f MB)." % (csv_file.size / (1000 * 1000),))
            error = True;
            return HttpResponseRedirect(reverse_lazy("gap:carga_usuario_masiva"))

        file_data = csv_file.read().decode("utf-8")

        lines = file_data.split("\n")
        lines = iter(lines)
        # loop over the lines and save them in db. If error , store as string and then display
        next(lines)
        for line in lines:
            if line:
                line = line.replace('\r', '')
                fields = line.split(";")
                data_dict = {"primer_nombre": fields[0], "segundo_nombre": fields[1], "apellido_materno": fields[2],
                             "apellido_paterno": fields[3], "email": fields[4], "rut": fields[5], "cargo": fields[6]}
                sucursal = Sucursal.objects.get(name=fields[7])
                data_dict["sucursal"] = sucursal.pk
                if fields[8] == 'Director':
                    field_8 = 'd'
                elif fields[8] == 'Operativo':
                    field_8 = 'o'
                elif fields[8] == 'Director-Operativo':
                    field_8 = 'do'
                else:
                    field_8 = 'nn'
                data_dict["tipo"] = field_8
                try:
                    form = EmpleadoForm(data_dict)
                    if form.is_valid():
                        form.save()
                    else:
                        messages.error(request,
                                       'Al menos uno de los empleados ingresados contiene un error en los campos')
                        error = True;
                        logging.getLogger("error_logger").error(form.errors.as_json())
                except Exception as e:
                    messages.error(request, 'Existio un error en el sistema')
                    error = True;
                    logging.getLogger("error_logger").error(form.errors.as_json())
                    pass

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        error = True;
        messages.error(request, "No se puede subir el archivo " + repr(e))
    if not error:
        messages.success(request, 'Success')
    return HttpResponseRedirect(reverse_lazy("gap:carga_usuario_masiva"))


# Crear Preguntas
class PreguntaListView(generic.ListView):
    model = Pregunta
    queryset = Pregunta.objects.all()
    paginate_by = 10


class PreguntaDetailView(generic.DetailView):
    model = Pregunta
    queryset = Pregunta.objects.all()


class PreguntaCreate(generic.CreateView):
    model = Pregunta
    form_class = PreguntaForm
    success_url = reverse_lazy('pregunta_list')


class PreguntaUpdate(generic.UpdateView):
    model = Pregunta
    form_class = PreguntaForm
    success_url = reverse_lazy('pregunta_list')


class PreguntaDelete(generic.DeleteView):
    model = Pregunta
    success_url = reverse_lazy('pregunta_list')


def Responsables(request):
    FormSet = modelformset_factory(Dominio, extra=0, form=DominioForm)
    if request.method == 'POST':
        form = FormSet(request.POST)
        if form.is_valid():
            form.save()
    context = FormSet()
    return render(request, "gap/responsables.html", {'formset': context})


class ParametrosUpdate(generic.UpdateView):
    model = Parametros
    form_class = ParametrosForm
    success_url = reverse_lazy('gap:home')


def Resultados(request):
    encuestas = Encuesta.objects.all()
    if request.method == 'POST':
        dominios = Dominio.objects.all()
        encuesta = Encuesta.objects.get(pk=request.POST.get("selectorencuesta", ""))
        encuestahasdoms = EncuestaHasDominio.objects.filter(encuesta=encuesta)
        Empleados = Empleado.objects.all()
        Empleados_count = Empleado.objects.all().count()
        query = RespuestasEncabezado.objects \
            .values('empleado', 'encuesta_has_dominio__dominio') \
            .annotate(Count('alternativas')) \
            .filter(encuesta_has_dominio__in=encuestahasdoms)
        queryDom2 = RespuestasEncabezado.objects \
            .values('empleado') \
            .annotate(Count('alternativas')) \
            .filter(encuesta_has_dominio__in=encuestahasdoms, encuesta_has_dominio__dominio_id=2)
        arr = numpy.zeros((Empleados_count, 11), numpy.int8)
        n = 0
        for empleado in Empleados:
            for x in query:
                if x['empleado'] == empleado.id:
                    if x['encuesta_has_dominio__dominio'] == 1:
                        arr[n][0] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 2:
                        arr[n][1] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 3:
                        arr[n][2] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 4:
                        arr[n][3] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 5:
                        arr[n][4] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 6:
                        arr[n][5] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 7:
                        arr[n][6] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 8:
                        arr[n][7] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 9:
                        arr[n][8] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 10:
                        arr[n][9] = x['alternativas__count']
                    if x['encuesta_has_dominio__dominio'] == 11:
                        arr[n][10] = x['alternativas__count']
            n += 1

        union = zip(Empleados, arr)
        context = {
            'dominios': dominios,
            'query': query,
            'union': union,
            'encuestas': encuestas,
            'encuesta': encuesta,
        }
        return render(request, "gap/resultados_list.html", context)

    context = {
        'encuestas': encuestas,

    }
    return render(request, "gap/resultados_list.html", context)


def encuestas(request):  # pagina principal de encuestas
    return render(request, "gap/i_encuestas.html")


def homecuestas(request, pk):
    llave = pk
    context = {
        'pk' : llave,
    }
    return render(request, "gap/i_encuestas.html", context)


def encuesta(request, dom_pk, enc_pk):
    get_object_or_404(Encuesta, pk=enc_pk, empleados__id__exact=1)
    encuestaxdom = EncuestaHasDominio.objects.get(dominio=dom_pk, encuesta=enc_pk)
    encabezadoResp = RespuestasEncabezado.objects.update_or_create(empleado_id=1, encuesta_has_dominio=encuestaxdom)
    AnswerFormSet = modelformset_factory(Alternativas, extra=100, form=AnswerForm)
    dominio = encuestaxdom.dominio
    Preguntas = Pregunta.objects.filter(dominio=encuestaxdom.dominio)
    if request.method == 'POST':
        answer_formset = AnswerFormSet(request.POST, queryset=Alternativas.objects.filter(Encabezado=encabezadoResp[0]))
        print(answer_formset.errors)
        if answer_formset.is_valid():
            answer_formset.save()

        else:
            reverse_lazy('gap:home')
    else:
        quest_id = 1
        question_data = [{'question': question,
                          'Encabezado': encabezadoResp[0]} for question in Preguntas]
        # answer_formset = AnswerFormSet(queryset=Alternativas.objects.filter(Encabezado=encabezadoResp[0]))
        answer_formset = AnswerFormSet(initial=question_data,
                                       queryset=Alternativas.objects.filter(Encabezado=encabezadoResp[0]))
    combined = zip(Preguntas, answer_formset)
    context = {
        'combined': combined,
        'answer_formset': answer_formset,
        'qname': dominio,
    }
    return render(request, 'gap/encuestas.html', context)


def Returnexcelresultados(request, pk):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="resultados.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Respuestas totales por dominio')
    hojaRespuestas = wb.add_sheet('Respuestas')
    hojaRespPorDia = wb.add_sheet('Respuestas por dia')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Apellido Paterno',
               'Apellido Materno',
               'Nombre',
               'Cargo',
               'Dominio 1',
               'Dominio 2',
               'Dominio 3',
               'Dominio 4',
               'Dominio 5',
               'Dominio 6',
               'Dominio 7',
               'Dominio 8',
               'Dominio 9',
               'Dominio 10',
               'Dominio 11',
               ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    Empleados = Empleado.objects.all().values_list('id',
                                                   'primer_nombre',
                                                   'apellido_materno',
                                                   'apellido_paterno',
                                                   'cargo')

    arr = numpy.zeros((len(Empleados), 11), numpy.int8)
    n = 0

    encuesta = Encuesta.objects.get(pk=pk)
    encuestahasdoms = EncuestaHasDominio.objects.filter(encuesta=encuesta)
    query = RespuestasEncabezado.objects \
        .values('empleado', 'encuesta_has_dominio__dominio') \
        .annotate(Count('alternativas')) \
        .filter(encuesta_has_dominio__in=encuestahasdoms)

    for empleado in Empleados:
        for x in query:
            if x['empleado'] == empleado[0]:
                if x['encuesta_has_dominio__dominio'] == 1:
                    arr[n][0] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 2:
                    arr[n][1] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 3:
                    arr[n][2] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 4:
                    arr[n][3] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 5:
                    arr[n][4] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 6:
                    arr[n][5] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 7:
                    arr[n][6] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 8:
                    arr[n][7] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 9:
                    arr[n][8] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 10:
                    arr[n][9] = x['alternativas__count']
                if x['encuesta_has_dominio__dominio'] == 11:
                    arr[n][10] = x['alternativas__count']
        n += 1
    a = arr.tolist()
    rows = zip(Empleados, a)

    for row in rows:
        row_num += 1
        terminocol = 0
        for col_num in range(len(row[0]) - 1):
            ws.write(row_num, col_num, str(row[0][col_num + 1]), font_style)
            terminocol += 1
        for col_num in range(len(row[1])):
            ws.write(row_num, (terminocol + col_num), str(row[1][col_num]), font_style)

    query2 = RespuestasEncabezado.objects \
        .values('empleado', 'alternativas__choice_text', 'alternativas__question', ) \
        .filter(encuesta_has_dominio__in=encuestahasdoms)
    Preguntas = Pregunta.objects.all()
    row_num = 0

    columns = ['Apellido Paterno',
               'Apellido Materno',
               'Nombre',
               'Cargo',
               ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    terminocol = 0
    for col_num in range(len(columns)):
        hojaRespuestas.write(0, col_num, columns[col_num], font_style)
        terminocol += 1
    for x in Preguntas:
        hojaRespuestas.write(0, terminocol, str(x.question_text), font_style)
        terminocol += 1

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    for empleado in Empleados:

        query2 = RespuestasEncabezado.objects \
            .values('empleado', 'alternativas__choice_text', 'alternativas__question', ) \
            .filter(encuesta_has_dominio__in=encuestahasdoms, empleado_id=empleado[0])
        row_num += 1
        hojaRespuestas.write(row_num, 0, str(empleado[1]), font_style)
        hojaRespuestas.write(row_num, 1, str(empleado[2]), font_style)
        hojaRespuestas.write(row_num, 2, str(empleado[3]), font_style)
        hojaRespuestas.write(row_num, 3, str(empleado[4]), font_style)
        for col_num in range(len(query2)):
            hojaRespuestas.write(row_num, (col_num + 4), str(query2[col_num]['alternativas__choice_text']), font_style)

    columns = ['Apellido Paterno',
               'Apellido Materno',
               'Nombre',
               'Cargo',
               ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    terminocol = 0

    delta = encuesta.fecha_termino.date() - encuesta.fecha_inicio.date()
    sdelta = encuesta.fecha_inicio.date()

    for col_num in range(len(columns)):
        hojaRespPorDia.write(0, col_num, columns[col_num], font_style)
        terminocol += 1
    for i in range(delta.days + 1):
        day = sdelta + timedelta(days=i)
        hojaRespPorDia.write(0, terminocol, str(str(day.day) + "-" + str(day.month)), font_style)
        terminocol += 1
    row_num = 0
    for empleado in Empleados:
        row_num += 1
        hojaRespPorDia.write(row_num, 0, str(empleado[1]), font_style)
        hojaRespPorDia.write(row_num, 1, str(empleado[2]), font_style)
        hojaRespPorDia.write(row_num, 2, str(empleado[3]), font_style)
        hojaRespPorDia.write(row_num, 3, str(empleado[4]), font_style)
        col_num = 0
        for i in range(delta.days + 1):
            day = sdelta + timedelta(days=i)
            query2 = RespuestasEncabezado.objects \
                .filter(encuesta_has_dominio__in=encuestahasdoms, empleado_id=empleado[0],
                        alternativas__creacion__date=day)
            print(len(query2))
            hojaRespPorDia.write(row_num, (col_num + 4), str(len(query2)),
                                 font_style)
            col_num += 1

    wb.save(response)
    return response


def Recomendaciones(request):
    encuestas = Encuesta.objects.all()
    if request.method == 'POST':
        encuesta = Encuesta.objects.get(pk=request.POST.get("selectorencuesta", ""))

        dominios = Dominio.objects.all()

        dominios_query = []
        parametros = Parametros.objects.get(pk=1)
        for dominio in dominios:
            preguntas = Pregunta.objects.filter(dominio=dominio)
            cumplimientoDom = 0
            deconocimiento_preguntaDom = 0
            dominio_query = []
            for pregunta in preguntas:
                cantSI = Alternativas.objects.filter(choice_text='s',
                                                     question=pregunta,
                                                     Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cantNO = Alternativas.objects.filter(choice_text='n',
                                                     question=pregunta,
                                                     Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cantDESC = Alternativas.objects.filter(choice_text='d',
                                                       question=pregunta,
                                                       Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cumplimiento_pregunta = 0
                deconocimiento_pregunta = 0
                if cantSI + cantNO + cantDESC > 0:
                    cumplimiento_pregunta = cantSI / (cantSI + cantNO + cantDESC)
                    deconocimiento_pregunta = cantDESC / (cantSI + cantNO + cantDESC)

                if cumplimiento_pregunta >= 0.5:
                    cumplimientoDom += 1
                deconocimiento_preguntaDom += deconocimiento_pregunta
            Porcentaje_Cumplimiento_Dom = int(cumplimientoDom / preguntas.count() * 100)
            dominio_query.append(dominio.nombre)
            dominio_query.append(str(Porcentaje_Cumplimiento_Dom))
            dominio_query.append(str(int(deconocimiento_preguntaDom / preguntas.count() * 100)))
            print("desc " + str(deconocimiento_preguntaDom) + "/ Preguntas " + str(preguntas.count()))
            if parametros.maximo_alto >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_alto:
                dominio_query.append('Alto')
            elif parametros.maximo_medio >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_medio:
                dominio_query.append('Medio')
            elif parametros.maximo_bajo >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_bajo:
                dominio_query.append('Bajo')
            else:
                dominio_query.append('Desconocido')
            dominio_query.append(dominio.recomendacion)
            dominio_query.append(dominio.referencias)
            dominios_query.append(dominio_query)

        context = {
            'encuestas': encuestas,
            'dominios': dominios_query,

        }
    else:
        context = {
            'encuestas': encuestas,

        }
    return render(request, "gap/recomendaciones_list.html", context)


def generarInforme(request, pk):
    encuesta = Encuesta.objects.get(pk=pk)

    dominios = Dominio.objects.all()

    dominios_query = []
    parametros = Parametros.objects.get(pk=1)
    for dominio in dominios:
        preguntas = Pregunta.objects.filter(dominio=dominio)
        cumplimientoDom = 0
        deconocimiento_preguntaDom = 0
        dominio_query = []
        recomendacion = "";
        for pregunta in preguntas:
            cantSI = Alternativas.objects.filter(choice_text='s',
                                                 question=pregunta,
                                                 Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cantNO = Alternativas.objects.filter(choice_text='n',
                                                 question=pregunta,
                                                 Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cantDESC = Alternativas.objects.filter(choice_text='d',
                                                   question=pregunta,
                                                   Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cumplimiento_pregunta = 0
            deconocimiento_pregunta = 0
            if cantSI + cantNO + cantDESC > 0:
                cumplimiento_pregunta = cantSI / (cantSI + cantNO + cantDESC)
                deconocimiento_pregunta = cantDESC / (cantSI + cantNO + cantDESC)

            if cumplimiento_pregunta >= 0.5:
                cumplimientoDom += 1
            else:
                if pregunta.recomendacion != None:
                    recomendacion += pregunta.recomendacion + "\n\t"
            deconocimiento_preguntaDom += deconocimiento_pregunta
        Porcentaje_Cumplimiento_Dom = int(cumplimientoDom / preguntas.count() * 100)
        dominio_query.append(dominio.nombre)
        dominio_query.append(Porcentaje_Cumplimiento_Dom)
        dominio_query.append(str(int(deconocimiento_preguntaDom / preguntas.count() * 100)))

        if parametros.maximo_alto >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_alto:
            dominio_query.append('Alto')
        elif parametros.maximo_medio >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_medio:
            dominio_query.append('Medio')
        elif parametros.maximo_bajo >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_bajo:
            dominio_query.append('Bajo')
        else:
            dominio_query.append('Desconocido')
        dominio_query.append(recomendacion)
        dominio_query.append(dominio.referencias)
        dominios_query.append(dominio_query)
    dominios_query = sorted(dominios_query, key=lambda dominios_query: dominios_query[1])
    context = {
        'dominios': dominios_query,

    }

    html_string = render_to_string('gap/informe.html', context)

    html = HTML(string=html_string)
    pdf = html.write_pdf();

    response = HttpResponse(pdf, content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=informe_recomendaciones.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    return response


def generarPlanDeAccion(request, pk):
    encuesta = Encuesta.objects.get(pk=pk)

    dominios = Dominio.objects.all()

    dominios_query = []
    parametros = Parametros.objects.get(pk=1)
    for dominio in dominios:
        preguntas = Pregunta.objects.filter(dominio=dominio)
        cumplimientoDom = 0
        deconocimiento_preguntaDom = 0
        dominio_query = []
        recomendacion = "";
        for pregunta in preguntas:
            cantSI = Alternativas.objects.filter(choice_text='s',
                                                 question=pregunta,
                                                 Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cantNO = Alternativas.objects.filter(choice_text='n',
                                                 question=pregunta,
                                                 Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cantDESC = Alternativas.objects.filter(choice_text='d',
                                                   question=pregunta,
                                                   Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
            cumplimiento_pregunta = 0
            deconocimiento_pregunta = 0
            if cantSI + cantNO + cantDESC > 0:
                cumplimiento_pregunta = cantSI / (cantSI + cantNO + cantDESC)
                deconocimiento_pregunta = cantDESC / (cantSI + cantNO + cantDESC)
            if cumplimiento_pregunta >= 0.5:
                cumplimientoDom += 1
            else:
                if pregunta.recomendacion != None:
                    recomendacion += pregunta.recomendacion + "\n\t"
            deconocimiento_preguntaDom += deconocimiento_pregunta
        Porcentaje_Cumplimiento_Dom = int(cumplimientoDom / preguntas.count() * 100)
        dominio_query.append(dominio.nombre)
        sla = 0
        if parametros.maximo_alto >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_alto:
            dominio_query.append('Baja')
            dominio_query.append(parametros.sla_alto)
            sla = parametros.sla_alto
        elif parametros.maximo_medio >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_medio:
            dominio_query.append('Media')
            dominio_query.append(parametros.sla_medio)
            sla = parametros.sla_medio
        elif parametros.maximo_bajo >= Porcentaje_Cumplimiento_Dom >= parametros.minimo_bajo:
            dominio_query.append('Alta')
            dominio_query.append(parametros.sla_bajo)
            sla = parametros.sla_bajo
        else:
            dominio_query.append('Desconocido')
        if dominio.responsable != None:
            dominio_query.append(dominio.responsable)
        else:
            dominio_query.append("Sin Asignar")
        dominio_query.append(recomendacion)
        date = encuesta.fecha_termino
        date += timedelta(days=1)
        dominio_query.append(date.date())
        date += timedelta(days=sla)
        dominio_query.append(date.date())
        dominios_query.append(dominio_query)
    dominios_query = sorted(dominios_query, key=lambda dominios_query: dominios_query[1])
    context = {
        'dominios': dominios_query,

    }

    html_string = render_to_string('gap/plan_de_accion.html', context)

    html = HTML(string=html_string)
    pdf = html.write_pdf();

    response = HttpResponse(pdf, content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=Plan_De_Accion.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    return response


def EncuestaCreate(request):
    if request.method == "POST":
        form = EncuestaForm(request.POST)
        if form.is_valid():
            new_encuesta = form.save()
            Dominios = Dominio.objects.all()
            for dominio in Dominios:
                EncuestaHasDominio.objects.create(dominio=dominio, encuesta=new_encuesta)
    else:
        form = EncuestaForm()
    return render(request, "gap/encuesta_form.html", {'form': form})


def Graficos(request):
    encuestas = Encuesta.objects.all()
    if request.method == 'POST':
        encuesta = Encuesta.objects.get(pk=request.POST.get("selectorencuesta", ""))
        dominios = Dominio.objects.all()
        dominiosquery = []

        dominios_query = []
        for dominio in dominios:
            preguntas = Pregunta.objects.filter(dominio=dominio)
            dominio_query = []
            dominio_query.append(dominio.nombre)
            dominiosquery.append(dominio.nombre)
            cumplimientoDom = 0
            porcentaje_preguntas = []
            for pregunta in preguntas:
                pregunt_query = []
                cantSI = Alternativas.objects.filter(choice_text='s',
                                                     question=pregunta,
                                                     Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cantNO = Alternativas.objects.filter(choice_text='n',
                                                     question=pregunta,
                                                     Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cantDESC = Alternativas.objects.filter(choice_text='d',
                                                       question=pregunta,
                                                       Encabezado__encuesta_has_dominio__encuesta=encuesta).count()
                cumplimiento_pregunta = 0
                if cantSI + cantNO + cantDESC > 0:
                    cumplimiento_pregunta = cantSI / (cantSI + cantNO + cantDESC)
                    pregunt_query.append(pregunta.pk)
                    pregunt_query.append(cumplimiento_pregunta)
                    porcentaje_preguntas.append(pregunt_query)
                if cumplimiento_pregunta >= 0.5:
                    cumplimientoDom += 1
            Porcentaje_Cumplimiento_Dom = int(cumplimientoDom / preguntas.count() * 100)
            dominio_query.append(Porcentaje_Cumplimiento_Dom)
            dominio_query.append(porcentaje_preguntas)
            dominios_query.append(dominio_query)
            sla = 0

        values = dominios_query
        categories = dominiosquery

        context = {
            'encuestas': encuestas,
            'categories': categories,
            'values': values,
        }
    else:
        context = {
            'encuestas': encuestas,

        }

    return render(request, 'gap/graficos.html', context)


def EncuestasShow(request):
    encuestas = Encuesta.objects.filter(empleados__id__exact=2)
    context = {
        'encuestas': encuestas,
    }
    return render(request, 'gap/encuestas_show.html', context)

# Carga Preguntas Masiva
def CargaMasivaPregunta(request):
    data = {}
    error = False
    if "GET" == request.method:
        return render(request, "gap/carga_pregunta_masiva.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'Archivo no es de formato CSV')
            error = True;
            return HttpResponseRedirect(reverse_lazy("gap:preguntas"))
        # if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request, "Archivo es excede el peso permitido (%.2f MB)." % (csv_file.size / (1000 * 1000),))
            error = True;
            return HttpResponseRedirect(reverse_lazy("gap:preguntas"))

        file_data = csv_file.read().decode("utf-8")

        lines = file_data.split("\n")
        lines = iter(lines)
        # loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            if line:
                line = line.replace('\r', '')
                fields = line.split(";")
                data_dict = {"question_text": fields[0], "recomendacion": fields[1], "pub_date": "2020-07-20"}
                dominio = Dominio.objects.get(pk=11)
                data_dict["dominio"] = dominio
                try:
                    form = PreguntaForm(data_dict)
                    print(form.errors)
                    if form.is_valid():
                        form.save()
                    else:
                        messages.error(request,
                                       'Al menos uno de los empleados ingresados contiene un error en los campos')
                        error = True;
                        logging.getLogger("error_logger").error(form.errors.as_json())
                except Exception as e:
                    messages.error(request, 'Existio un error en el sistema')
                    error = True;
                    logging.getLogger("error_logger").error(form.errors.as_json())
                    pass

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. " + repr(e))
        error = True;
        messages.error(request, "No se puede subir el archivo " + repr(e))
    if not error:
        messages.success(request, 'Success')
    return HttpResponseRedirect(reverse_lazy("gap:preguntas"))