from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(Departamento) 
class DepartamentoAdmin(admin.ModelAdmin):
    pass
@admin.register(Sucursal) 
class SucursalAdmin(admin.ModelAdmin):
    pass

@admin.register(Empleado) 
class EmpleadoAdmin(admin.ModelAdmin):
    pass

@admin.register(Pregunta) 
class PreguntaAdmin(admin.ModelAdmin):
    pass

@admin.register(Alternativas) 
class AlternativasAdmin(admin.ModelAdmin):
    pass
@admin.register(Parametros) 
class ParametrosAdmin(admin.ModelAdmin):
    pass
@admin.register(Dominio) 
class DominioAdmin(admin.ModelAdmin):
    pass
@admin.register(RespuestasEncabezado) 
class RespuestasEncabezadoAdmin(admin.ModelAdmin):
    pass
@admin.register(Encuesta)
class EncuestaAdmin(admin.ModelAdmin):
    pass
@admin.register(EncuestaHasDominio)
class EncuestaHasDominioAdmin(admin.ModelAdmin):
    pass