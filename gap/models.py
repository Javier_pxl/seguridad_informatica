from datetime import timezone, datetime

from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Sucursal(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Departamento(models.Model):
    name = models.CharField(max_length=30)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Empleado(models.Model):
    user = models.OneToOneField(User,null=True, blank=True, on_delete=models.CASCADE)
    primer_nombre = models.CharField(max_length=50)
    segundo_nombre = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    rut = models.CharField(max_length=50)
    cargo = models.CharField(max_length=50)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)

    LOAN_TYPE = (
        ('d', 'Director'),
        ('o', 'Operativo'),
        ('do', 'Director-Operativo'),
    )
    tipo = models.CharField(max_length=20, choices=LOAN_TYPE, blank=True, default='o', help_text='Tipo')

    class Meta:
        permissions = (("is_employ", "employ"),)  
    
    def __str__(self):
        return '{primer_nombre} {segundo_nombre}'.format(primer_nombre=self.primer_nombre,
                                                         segundo_nombre=self.segundo_nombre)


class Encuesta(models.Model):
    fecha_inicio = models.DateTimeField()
    fecha_termino = models.DateTimeField()
    empleados = models.ManyToManyField(Empleado)

    def __str__(self):
        return '{fecha_inicio} A {fecha_termino}'.format(fecha_inicio=self.fecha_inicio.date(),
                                                         fecha_termino=self.fecha_termino.date())


class Dominio(models.Model):
    nombre = models.CharField(max_length=100)
    responsable = models.ForeignKey(Empleado, on_delete=models.CASCADE, null=True, blank=True)
    recomendacion = models.CharField(max_length=300, blank=True)
    referencias = models.CharField(max_length=300, blank=True)

    def __str__(self):
        return '{dominio}'.format(dominio=self.nombre)


class EncuestaHasDominio(models.Model):
    dominio = models.ForeignKey(Dominio, on_delete=models.CASCADE)
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE)

    def __str__(self):
        return '{dominio} - {encuesta}'.format(dominio=self.dominio, encuesta=self.encuesta)


class RespuestasEncabezado(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
    encuesta_has_dominio = models.ForeignKey(EncuestaHasDominio, on_delete=models.CASCADE)

    def __str__(self):
        return '{empleado} - {encuesta_has_dominio}'.format(empleado=self.empleado,
                                                           encuesta_has_dominio=self.encuesta_has_dominio)


class Pregunta(models.Model):
    question_text = models.CharField(max_length=500)
    dominio = models.ForeignKey(Dominio, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date published')
    recomendacion = models.CharField(max_length=500, null=True, blank=True)

    def get_absolute_url(self):
        return reverse('pregunta_detail', args=[str(self.id)])

    def __str__(self):
        return '{question}'.format(question=self.question_text)


# Respuestas detalle
class Alternativas(models.Model):
    question = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    Encabezado = models.ForeignKey(RespuestasEncabezado, on_delete=models.CASCADE, blank=True, null=True)
    creacion = models.DateTimeField('date published', blank=True, null=True)

    LOAN_TYPE = (
        ('s', 'Si'),
        ('n', 'No'),
        ('d', 'desconozco'),
    )
    choice_text = models.CharField(max_length=20, choices=LOAN_TYPE, blank=True, default='d', help_text='Tipo')

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.creacion = datetime.now()
        return super(Alternativas, self).save(*args, **kwargs)

    def __str__(self):
        return self.choice_text

    class Meta:
        verbose_name = 'alternativa'
        verbose_name_plural = 'alternativas'


class Parametros(models.Model):
    sla_bajo = models.IntegerField(blank=False, default=20)
    sla_medio = models.IntegerField(blank=False, default=30)
    sla_alto = models.IntegerField(blank=False, default=40)
    minimo_bajo = models.IntegerField(blank=False, default=20)
    minimo_medio = models.IntegerField(blank=False, default=30)
    minimo_alto = models.IntegerField(blank=False, default=40)
    maximo_bajo = models.IntegerField(blank=False, default=20)
    maximo_medio = models.IntegerField(blank=False, default=30)
    maximo_alto = models.IntegerField(blank=False, default=40)
